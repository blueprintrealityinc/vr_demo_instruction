---

# 山东大学IRC实验室暑期学校 VR/AR DIY

### 本Demo共包含四个实验，请任选其一完成不同难度的任务。

1. 使用微软“**混合现实门户**”提供的虚拟现实模拟器测试并调试Demo
2. 注册Bitbucket账户并登录。
3. 使用*git fork* fork其中一个repository。
4. 使用*git clone*将已经fork的repository clone到本地并进行开发。
5. 使用**Unity 2018+**版本配合Steam与SteamVR进行VR开发
6. 在模拟器上调试成功后，编译成*x86_64*运行程序，在实验室提供的Vive上进行真机测试。

### Important Note： 

1. 在模拟器中调试时，可以使用*PageUp*和*PageDown*调整高度！！！！！！！！不知道这个的话 没法完成！
2. 选择CutieKeys的同学请务必从git上的项目开始，先导手册自带的UnityPackage有一个隐含bug！
3. XRInputDeviceState: Failed to set the value at featureIndex [4]. 如有这一Error的同学，可以删除project中的Library, Temp, obj, Packages文件夹并转回使用Unity 2018.1.X版本。

---

## CutieKeys


[项目地址](https://bitbucket.org/blueprintrealityinc/vr_demo_cutiekeys)

简介： 可以使用 VR 控制器，敲击虚拟键盘，输出字母。

调试难度： ★★★

基于CutieKeys，完成以下tasks：

1. 制作 VR 钢琴   （难度： ★）
2. 制作 VR 架子鼓 （难度： ★★）
3. 制作 VR 打地鼠 （难度： ★★★）

![CutieKeys](https://bitbucket.org/blueprintrealityinc/vr_demo_instruction/raw/master/demo1.png "快来愉快地开始你的第一个VR App吧")

---

## SceneNavigation


[项目地址](https://bitbucket.org/blueprintrealityinc/vr_demo_scenenavigation)

简介： 带有 VR 摄像头的 3D 场景。

特殊Notes: 

由于之前使用的教程内容不再适用，现有两个办法，fork的同学可以选择更新并merge，如果git操作不够熟练的，可以保持原工程，但需拖入SteamVR->InteractionSystem->Core->Prefabs->Player.prefab，并删除场景中的[CameraRig]，如果喜欢之前Camera的特殊效果，找到[CameraRig]->Camera(Head)->Camera(eye)，将其上绑定的Image Effect（Fog,DepthField，Sunshaft等等）绑定到Player->SteamVRObjects->VRCamera。

对于Teleport Area，要求自制floor，可以选择GameObject->3D Objects->Plane，创建一个Plane，将高度y设为0.5，将scale（缩放）设为25，1，25，并将Teleport Area绑定其上即可当做自由传送区域使用。
Teleport Point要求特定传送点，Teleport Area则是一整个范围内都可以传送，可以自行选择，并作出有意思的漫游App。

当在“混合现实门户”中的头盔模拟器中调试时，右键点击Left Controller和Right Controller下的Touchpad圆盘，将会显示Teleport传送线，保持右键按住并向上滑动后松开可以传送至想到达的区域。

调试难度： ★

特殊Notes: 

由于之前使用的教程内容不再适用，现有两个办法，fork的同学可以选择更新并merge，如果git操作不够熟练的，可以保持原工程，但需拖入SteamVR->InteractionSystem->Core->Prefabs->Player.prefab，并删除场景中的[CameraRig]，如果喜欢之前Camera的特殊效果，找到[CameraRig]->Camera(Head)->Camera(eye)，将其上绑定的Image Effect（Fog,DepthField，Sunshaft等等）绑定到Player->SteamVRObjects->VRCamera。

对于Teleport Area，要求自制floor，可以选择GameObject->3D Objects->Plane，创建一个Plane，将高度y设为0.5，将scale（缩放）设为25，1，25，并将Teleport Area绑定其上即可当做自由传送区域使用。
Teleport Point要求特定传送点，Teleport Area则是一整个范围内都可以传送，可以自行选择，并作出有意思的漫游App。

当在“混合现实门户”中的头盔模拟器中调试时，右键点击Left Controller和Right Controller下的Touchpad圆盘，将会显示Teleport传送线，保持右键按住并向上滑动后松开可以传送至想到达的区域。


基于提供的场景，完成以下tasks：

1. 根据[教程](https://unity3d.college/2017/05/16/steamvr-locomotion-teleportation-movement/)， 完成 *VR Teleport* （难度： ★★）
2. 自学并尝试使用 Unity Terrain 制作工具，绘制地形并种植植被，并在新构建的场景上完成 VR 漫游 （难度： ★★★）
3. 在场景里加入更多其他3D物件，并增加更多交互 （如VR射箭） （难度： ★★★★）

![SceneNavigation](https://bitbucket.org/blueprintrealityinc/vr_demo_instruction/raw/master/demo2.png "快来愉快地开始你的第一个VR App吧")

---

## Drag And Drop


[项目地址](https://bitbucket.org/blueprintrealityinc/vr_demo_draganddrop)

简介： 按下 VR 控制器的 Trigger 按键，可以捡起小球并投掷。

调试难度： ★★★★★

基于提供的场景，完成以下tasks：

1. 设计并完成 VR 保龄球    （难度： ★★）
2. 设计并完成 VR 投篮游戏  （难度： ★★★）
3. 设计并完成 VR 3D 拼图   （难度： ★★★★）
4. 制作 会闪避躲藏的 AI 并完成 VR 躲避球游戏 （难度： ★★★★★）

![Drag And Drop](https://bitbucket.org/blueprintrealityinc/vr_demo_instruction/raw/master/demo3.png "快来愉快地开始你的第一个VR App吧")

---

## Voice


[项目地址](https://bitbucket.org/blueprintrealityinc/vr_demo_voice)

简介： 进入设置->时间和语言->语音，配置语音识别，打开cortana进行设置。 之后进入项目，向 Amane Kisora-chan 进行问好（Hello或你好），将会得到回答。

调试难度： 无

基于提供的场景，完成以下tasks：

1. 替换喜欢的模型并完成更多的语音识别应答（Handler） （难度： ★★）
2. 参考Unity官方[STT](https://bitbucket.org/Unity-Technologies/speech-to-text)，替换默认的Windows Speech Recognition Service。（目前新版本IBM Watson由于IBM SDK代码更新且不向下兼容，调试难度较高，不建议尝试） （难度： ★★★★★）
3. 使用[RT-Voice](https://assetstore.unity.com/packages/tools/audio/rt-voice-48394)替换默认Windows TTS服务（如果你会选择这个难度的任务，那么我相信这个插件的免费版对你而言难度也不大了吧） （难度： ★★★★★）

![Voice](https://bitbucket.org/blueprintrealityinc/vr_demo_instruction/raw/master/demo4.png "快来愉快地开始你的第一个VR App吧")

---